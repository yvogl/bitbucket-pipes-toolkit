import os
from unittest import TestCase
from uuid import uuid4

import requests

from bitbucket_pipes_toolkit import CodeInsights


def create_test_report():
    report = {
        "type": "report",
        "report_type": "BUG",
        "title": "Bug report",
        "details": "This bug report is auto generated by bug tool.",
        "result": "FAILED",
        "reporter": "Created by atlassians bug tool.",
        "external_id": str(uuid4()),
        "logoUrl": "https://bug-tool.atlassian.com/logo.png",
    }
    return report


def create_test_annotation():
    annotation = {
        "annotation_type": "BUG",
        "external_id": str(uuid4()),
        "summary": "This line has a bug!",
        "details": "This is a really bad bug, caused by IEEE09999, you need to ensure arrays dont go out of bounds.",
        "severity": "HIGH",
        "result": "FAILED",
        "link": "https://bug-tool.atlassian.com/report/10/bug/100",
    }
    return annotation


DEFAULT_COMMIT = '2cb65bb'


class BaseTestCase(TestCase):
    def get_commit(self):
        if os.getenv('TEST_COMMIT'):
            return os.getenv('TEST_COMMIT')
        return DEFAULT_COMMIT

    def get_insights_client(self):
        insights = CodeInsights('bitbucket-pipes-toolkit',
                                auth_type='access_token',
                                account=self.account,
                                token=self.token,
                                username="bitbucketpipelines")
        return insights

    def setUp(self):
        self.account = os.getenv('BITBUCKET_ACCOUNT')
        self.username = os.getenv('BITBUCKET_USERNAME')
        self.token = os.getenv('BITBUCKET_ACCESS_TOKEN')
        insights = self.get_insights_client()
        reports = insights.get_reports(self.get_commit())
        for report in reports['values']:
            insights.delete_report(self.get_commit(), report['uuid'])

    def tearDown(self):
        super().tearDown()
        insights = self.get_insights_client()
        reports = insights.get_reports(self.get_commit())
        for report in reports['values']:
            insights.delete_report(self.get_commit(), report['uuid'])


class CodeInsightsTestCase(BaseTestCase):

    def test_create_report_successful(self):
        report = create_test_report()
        insights = self.get_insights_client()
        report = insights.create_report(self.get_commit(), report_data=report)

        self.assertEqual(report['external_id'], report['external_id'])

    def test_get_report_successful(self):
        report = create_test_report()
        insights = self.get_insights_client()
        report = insights.create_report(self.get_commit(), report_data=report)

        report_response = insights.get_report(
            self.get_commit(), report['uuid'])
        self.assertDictEqual(report_response, report)

    def test_create_report_miss_report_id_return_bad_request(self):
        wrong_report = {}
        insights = self.get_insights_client()

        with self.assertRaisesRegex(requests.exceptions.HTTPError, '400 Client Error: Bad Request'):
            insights.create_report(self.get_commit(), report_data=wrong_report)

    def test_create_report_wrong_commit_return_not_found(self):
        report = create_test_report()
        insights = self.get_insights_client()

        with self.assertRaisesRegex(requests.exceptions.HTTPError, '404 Client Error: Not Found'):
            insights.create_report('0000000', report_data=report)


class AuthlessTestCase(BaseTestCase):
    def get_commit(self):
        return 'd1b068a'

    def get_insights_client(self):
        insights = CodeInsights('bitbucket-pipes-toolkit',
                                auth_type='authless',
                                username='bitbucketpipelines',
                                auth_proxy_host='localhost')
        return insights

    def test_authless_create_report_successful_(self):
        report = create_test_report()
        insights = self.get_insights_client()
        report = insights.create_report(self.get_commit(), report_data=report)

        self.assertEqual(report['external_id'], report['external_id'])


class CreateAnnotationTestCase(BaseTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()

        report_data = create_test_report()
        self.insights = self.get_insights_client()
        self.report = self.insights.create_report(
            self.get_commit(), report_data=report_data)

    def test_create_annotation_successful(self):
        annotation_data = create_test_annotation()

        annotation = self.insights.create_annotation(
            self.get_commit(), self.report['uuid'], annotation_data)

        self.assertEqual(
            annotation_data['external_id'], annotation['external_id'])

    def test_create_annotation_miss_annotation_id_in_data_return_bad_request(self):
        wrong_annotation_data = {}
        with self.assertRaisesRegex(requests.exceptions.HTTPError, '400 Client Error: Bad Request'):
            self.insights.create_annotation(
                self.get_commit(), self.report['uuid'], wrong_annotation_data)

    def test_create_annotation_wrong_commit_return_not_found(self):
        annotation_data = create_test_annotation()
        with self.assertRaisesRegex(requests.exceptions.HTTPError, '404 Client Error: Not Found'):
            self.insights.create_annotation(
                '0000000', self.report['uuid'], annotation_data)
