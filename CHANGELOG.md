# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 4.4.1

- patch: Internal maintenance: redo skip test code insights authless.

## 4.4.0

- minor: Add option for Codeinsights to authenticate via BITBUCKET_ACCESS_TOKEN.
- patch: Internal maintenance: Bump pipes versions in pipelines config file.
- patch: Internal maintenance: Update annotations tests to use BITBUCKET_ACCESS_TOKEN.

## 4.3.0

- minor: Internal maintenance: Upgrade packages versions: GitPython to 3.1.41.

## 4.2.1

- patch: Update documentation.

## 4.2.0

- minor: Internal maintenance: Upgrade packages versions: GitPython to 3.1.36.

## 4.1.0

- minor: Internal maintenance: Upgrade packages versions: GitPython to 3.1.34.

## 4.0.0

- major: Internal maintenance: Upgrade packages versions: GitPython to 3.1.32, docker to 6.1.3, PyYAML to 6.0.1.
- patch: Internal maintenance: update python docker image to 3.10, and pipes to latest versions in pipelines config file.

## 3.3.1

- patch: Internal maintenance: Update read-the-docs python dependencies to version 3.10.

## 3.3.0

- minor: Added support to resolve auth method in toolkit.

## 3.2.1

- patch: Update README code example.

## 3.2.0

- minor: Fix typos in docs.

## 3.1.0

- minor: Add update docker workdir to test.

## 3.0.0

- major: Loose constraints on toolkit package installing.

## 2.2.0

- minor: Ignore loading string type variables in YAML.

## 2.1.0

- minor: Bump PyYAML to 5.4.

## 2.0.1

- patch: Fix documentation typos.

## 2.0.0

- major: Internal maintenance: update packages dependecies: docker 4.2, pyyaml 5.3.

## 1.14.2

- patch: Internal maintenance: comparing versions from official released pipes

## 1.14.1

- patch: Internal maintenance: fix version parsing

## 1.14.0

- minor: Added support for authless code insights

## 1.13.0

- minor: Fix get pipelines variables for tests

## 1.12.0

- minor: Internal maintenance: Upgrade dependency version pyyaml

## 1.11.0

- minor: Internal maintenance: Upgrade dependency version GitPython.

## 1.10.0

- minor: Add link to create community question when pipe fails.
- minor: Added the API to interact with code insights in Bitbucket

## 1.9.0

- minor: Allow metadata to be read from file

## 1.8.0

- minor: Add helper for getting current pipeline url

## 1.7.3

- patch: Update warning message when new pipe version is available.

## 1.7.2

- patch: Internal maintenance: Added flake8 check

## 1.7.1

- patch: Internal maintenance: Increased test coverage

## 1.7.0

- minor: Added a new option to check for newer pipe versions

## 1.6.4

- patch: Add support for custom volumes in test

## 1.6.3

- patch: Changed the INFO log message color to Blue

## 1.6.2

- patch: Fixed a missing dependency

## 1.6.1

- patch: Internal maintenance: Add test validation special chars.

## 1.6.0

- minor: enable_debug_log_level is not invoked when the Pipe is created

## 1.5.1

- patch: Internal release

## 1.5.0

- minor: Improved testing utilities

## 1.4.0

- minor: Fixed an issue with special characters in variables

## 1.3.1

- patch: Updated the package metadata

## 1.3.0

- minor: New logging methods were added to the main Pipe interface

## 1.2.0

- minor: Added support for decompiling array variables
- patch: Internal maintenance

## 1.1.1

- patch: Documentation updates

## 1.1.0

- minor: Shared pipes directory helpers were added

## 1.0.0

- major: Pipe initialization was made generic to improve testebility
- minor: Added support for array variables in pipes
- minor: PipeTestCase now automatically injects pipelines variables into each test container

## 0.7.0

- minor: Improved the base Pipe

## 0.6.2

- patch: Add BitbucketApiRepositoriesPipelines to __all__

## 0.6.1

- patch: Fixed the long description

## 0.6.0

- minor: Added helper to work with Bitbucket Pipelines API

## 0.5.0

- minor: Imvroved the base Pipe

## 0.4.1

- patch: Update a validation error message

## 0.4.0

- minor: Fix bug with a missing variable

## 0.3.0

- minor: Use yaml to load env variables

## 0.2.0

- minor: Added a Pipe base class
- minor: Fixed the bug in enable_debug
- minor: Impoved the PipeTestCase class

## 0.1.3

- patch: Fixed enable_debug function
- patch: Fixed installation scripts

## 0.1.2

- patch: Fix name error

## 0.1.1

- patch: Fixed missing dependencies

## 0.1.0

- minor: Change the name of the package

## 0.0.3

- patch: Fixing CI

## 0.0.2

- patch: Version bump

## 0.0.1

- patch: Initial
